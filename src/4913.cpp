#include <iostream>
#include <vector>
#include <utility>
#include <cassert>
#include <string>

using namespace std;

int get_depth(int root_id, const vector<pair<int, int>>& leaf_of)
{
    int root_index = root_id - 1;
    // cout << "root_index = " << root_index << endl;
    assert(root_index < leaf_of.size());
    
    int left_subroot_id = leaf_of[root_index].first;
    int right_subroot_id = leaf_of[root_index].second;
    if(left_subroot_id == 0 && right_subroot_id == 0)
    {
        return 1;
    }
    int left_depth = left_subroot_id == 0 ? 0 : get_depth(left_subroot_id, leaf_of);
    int right_depth = right_subroot_id == 0 ? 0 : get_depth(right_subroot_id, leaf_of);
    return max(left_depth, right_depth) + 1;
}

int main()
{
    
    string node_cnt_str;
    cin >> node_cnt_str;
    int node_cnt = stoi(node_cnt_str);
    
    int input_cnt = 0;
    string temp_leaf_id_str;
    int temp_leaf_id = 0;
    vector<pair<int, int>> leaf_of(node_cnt);

    // cout << "leaf_of size = " << leaf_of.size() <<endl;
    while(cin >> temp_leaf_id_str)
    {
        temp_leaf_id = stoi(temp_leaf_id_str);
        int curr_node_id = input_cnt / 2;
        int column = input_cnt % 2;
        if(column == 0)
        {
            leaf_of[curr_node_id].first = temp_leaf_id;
        }
        else
        {
            leaf_of[curr_node_id].second = temp_leaf_id;
        }
        ++input_cnt;
    }
    
    cout << get_depth(1, leaf_of) << endl;
    
    return 0;
}