#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define DIRS 9

const int offsets[DIRS][2] = {{0, 0},{-2, -1},{-1, -2},{-2, 1},{-1, 2},{2, -1},{1, -2},{1, 2},{2, 1}};

int main()
{
    string str;
    cin >> str;
    int x_b = stoi(str);
    cin >> str;
    int y_b = stoi(str);
    cin >> str;
    int x_h = stoi(str);
    cin >> str;
    int y_h = stoi(str);
    
    vector<vector<long long>> dp(x_b + 1, vector<long long>(y_b + 1, 0));
    
    for(int dir = 0; dir < DIRS; ++dir)
    {
        int x_ban = x_h + offsets[dir][0];
        int y_ban = y_h + offsets[dir][1];
        if(x_ban >= 0 && x_ban <= x_b && y_ban >= 0 && y_ban <= y_b)
        {
            dp[x_ban][y_ban] = -1;
        }
    }

    dp[0][0] = 1;

    for(int col = 1; col <= y_b; ++col)
    {
        if(dp[0][col] == -1) continue;
        dp[0][col] = max(dp[0][col - 1], 0LL);
    }
    for(int row = 1; row <= x_b; ++row)
    {
        if(dp[row][0] == -1) continue;
        dp[row][0] = max(dp[row - 1][0], 0LL);
    }
    
    
    
    for(int row = 1; row <= x_b; ++row)
    {
        for(int col = 1; col <= y_b; ++col)
        {
            if(dp[row][col] == -1) continue;
            dp[row][col] = max(dp[row - 1][col], 0LL) + max(dp[row][col - 1], 0LL);
        }
    }
    
    // for(int row = 0; row <= x_b; ++row)
    // {
    //     for(int col = 0; col <= y_b; ++col)
    //     {
    //         cout << dp[row][col] << " ";
    //     }
    //     cout << endl;
    // }

    cout << max(dp[x_b][y_b], 0LL); 
    
    return 0;
}