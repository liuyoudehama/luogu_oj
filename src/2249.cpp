#include <iostream>
#include <vector>
#include <utility>
#include <cassert>
#include <string>
#include <cmath>
#include <set>

using namespace std;

int get_first_occurance(const vector<int>& numbers, int target)
{
    assert(numbers.size() > 0);

    if(numbers[0] == target) return 1;
    


    int lower_bound = 0;
    int upper_bound = numbers.size();
    
    //find last number that lower than target
    while(lower_bound + 1 < upper_bound)
    {
        int mid = lower_bound + (upper_bound - lower_bound)/2;
        if(numbers[mid] >= target)
        {
            upper_bound = mid;
        }
        else
        {
            lower_bound = mid;
        }

        // cout << lower_bound << endl;
        // cout << upper_bound << endl;
        // cout << "," << endl;
    }

    if((lower_bound == 0 && numbers[0] > target) || lower_bound == numbers.size() - 1 || numbers[lower_bound + 1] != target)
    {
        return -1;
    }

    return lower_bound + 1 + 1;
}

int main()
{
    string str;
    cin >> str;
    int number_cnt = stoi(str);
    
    cin >> str;
    int query_cnt = stoi(str);
    
    vector<int> numbers(number_cnt);
    for(int cnt = 0; cnt < number_cnt; ++cnt)
    {
        cin >> str;
        numbers[cnt] = stoi(str);
    }

    for(int cnt = 0; cnt < query_cnt; ++cnt)
    {
        cin >> str;
        cout << get_first_occurance(numbers, stoi(str)) << " ";
    }

    return 0;
}