#include <iostream>
#include <vector>
#include <utility>
#include <cassert>
#include <string>
#include <cmath>

using namespace std;


int build_tree(string& tree_node_values,
    vector<pair<int, int>>& tree_node_childs,
    const string& in_order_str, 
    const string& pre_order_str, 
    int in_order_start, 
    int in_order_end, 
    int pre_order_start, 
    int pre_order_end)
{
    if(in_order_start == in_order_end)
    {
        return -1;
    }

    // cout << "build_tree:" << endl;
    // cout << "in_order_start = " << in_order_start << endl;
    // cout << "in_order_end = " << in_order_end << endl;
    // cout << "pre_order_start = " << pre_order_start << endl;
    // cout << "pre_order_end = " << pre_order_end << endl;
    // cout << endl;
    // cout << endl;

    assert(in_order_start >= 0);
    assert(in_order_end >= 0);
    assert(pre_order_start >= 0);
    assert(pre_order_end >= 0);

    char root = pre_order_str[pre_order_start];
    tree_node_values.push_back(root);
    int root_id = tree_node_values.size() - 1;

    tree_node_childs.push_back(pair<int, int>(-1, -1));
    
    auto root_pos_inorder = in_order_str.find(root);
    int left_part_elem_cnt = root_pos_inorder - in_order_start;
    // int right_part_elem_cnt = in_order_end - (root_pos_inorder + 1);

    tree_node_childs[root_id].first = build_tree(tree_node_values, tree_node_childs, in_order_str, pre_order_str, in_order_start, root_pos_inorder, pre_order_start + 1, pre_order_start + 1 + left_part_elem_cnt);
    tree_node_childs[root_id].second = build_tree(tree_node_values, tree_node_childs, in_order_str, pre_order_str, root_pos_inorder + 1, in_order_end, pre_order_start + 1 + left_part_elem_cnt, pre_order_end);

    return root_id;
}

void post_order_print(int root_id, const string& tree_node_values, const vector<pair<int, int>>& tree_node_childs)
{
    if(root_id == -1)
    {
        return;
    }

    int left_id = tree_node_childs[root_id].first;
    int right_id = tree_node_childs[root_id].second;
    post_order_print(left_id, tree_node_values, tree_node_childs);
    post_order_print(right_id, tree_node_values, tree_node_childs);
    cout << tree_node_values[root_id];
}

void generate_random_tree(string& tree_node_values, vector<pair<int, int>>& tree_node_childs)
{

}

void test()
{

}

int main()
{
    string in_order_str;
    string pre_order_str;
    cin >> in_order_str;
    cin >> pre_order_str;

    string tree_node_values;
    vector<pair<int, int>> tree_node_childs;

    int root_id = build_tree(tree_node_values, tree_node_childs, in_order_str, pre_order_str, 0, in_order_str.size(), 0, pre_order_str.size());
    
    post_order_print(root_id, tree_node_values, tree_node_childs);

    return 0;
}