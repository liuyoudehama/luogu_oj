#include <iostream>
#include <vector>
#include <utility>
#include <cassert>
#include <string>
#include <cmath>

using namespace std;

int get_2nd_winner(vector<int>& winners, const vector<int>& team_values, int total_round)
{
    // cout << total_round << endl;
    for (int round_index = 0; round_index < total_round - 1; round_index++)
    {
        int round = round_index + 1;
        
        // cout << "round = " << round << endl;

        int last_interval = pow(2, round - 1);
        int interval = pow(2, round);
        int total_match_curr_round = pow(2, total_round - round);
        for(int match_id = 0; match_id < total_match_curr_round; ++match_id)
        {
            // cout << "match_id = " << match_id << endl;
            
            int candidate_1_id = match_id * interval;
            int candidate_2_id = match_id * interval + last_interval;
            
            // cout << winners[candidate_1_id] << "(" << team_values[winners[candidate_1_id]] <<  ")" << " vs " << winners[candidate_2_id] << "(" << team_values[winners[candidate_2_id]] <<  ")" << endl;
            
            if(team_values[winners[candidate_1_id]] < team_values[winners[candidate_2_id]])
            {
                // cout << "lose" << endl;
                winners[candidate_1_id] = winners[candidate_2_id];
            }
        }
    }

    return team_values[winners[0]] < team_values[winners[winners.size() / 2]] ? winners[0] : winners[winners.size()/2];
}

int main()
{
    string N_str;
    cin >> N_str;
    int total_round = stoi(N_str);
    
    string temp_team_value_str;
    vector<int> team_values;
    team_values.reserve(pow(2, total_round));
    vector<int> winners(pow(2, total_round), 0);
    
    for(int i = 0; i < winners.size(); ++i)
    {
        winners[i] = i;
    }

    while(cin >> temp_team_value_str)
    {
        team_values.push_back(stoi(temp_team_value_str));
    }
    
    cout << get_2nd_winner(winners, team_values, total_round) + 1 << endl;
    
    return 0;
}