#include <iostream>
#include <vector>
#include <utility>
#include <cassert>
#include <string>
#include <algorithm>

using namespace std;

int get_first_lower(const vector<int>& trees, int target)
{
    int lower_bound = 0;
    int upper_bound = trees.size();
    
    while(lower_bound + 1 < upper_bound)
    {
        int mid = lower_bound + (upper_bound - lower_bound)/2;
        if(trees[mid] >= target)
        {
            upper_bound = mid;
        }
        else
        {
            lower_bound = mid;
        }
    }

    return trees[lower_bound] < target ? lower_bound + 1: lower_bound;
}

int main()
{   
    string str;
    cin >> str;
    int tree_cnt = stoi(str);

    cin >> str;
    int length_required = stoi(str);

    vector<int> trees(tree_cnt);
    
    for (int i = 0; i < tree_cnt; i++)
    {
        cin >> str;
        trees[i] = stoi(str);
    }
    
    sort(trees.begin(), trees.end());
    int max_length = trees.back();
    vector<long long> subfix_sum(trees.size() + 1, 0);

    for(int i = subfix_sum.size() - 2; i >= 0; i--)
    {
        subfix_sum[i] = subfix_sum[i + 1] + trees[i];
        // cout << "subfix_sum[i] = " << subfix_sum[i] << endl;
    }

    //[lower, upper)
    int length_lower = 0;
    int length_upper = max_length;

    while (length_lower + 1 < length_upper)
    {
        // cout << length_lower << endl;
        // cout << length_upper << endl;
        // cout << "," << endl;

        int length_mid = length_lower + (length_upper - length_lower)/2;
        int first_lower = get_first_lower(trees, length_mid);
        
        // cout << "first_lower = " << first_lower <<endl;
        // cout << "subfix_sum[first_lower] = " << subfix_sum[first_lower] <<endl;
        // cout << "(trees.size() - first_lower) * length_mid = " << (trees.size() - first_lower) * length_mid <<endl;

        long long length_get = subfix_sum[first_lower] - (trees.size() - first_lower) * length_mid;
        
        if(length_get >= length_required)
        {
            length_lower = length_mid;
        }
        else
        {
            length_upper = length_mid;
        }
    }
    
    cout << length_lower;

    return 0;
}