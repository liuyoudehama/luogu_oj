#!/bin/bash

usage() {
    echo "Usage: $0 <question_number>"
    exit 1
}

new_question() {
    number=$1
    cp template/template.cpp src/$number.cpp
    touch inputs/input_$number
}

# Check the number of arguments
if [ "$#" -ne 1 ]; then
    echo "arguments number not correct: $# provided, 1 required."
    usage
else
    new_question $@
    echo "done."
fi