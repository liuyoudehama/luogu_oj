#!/bin/bash

# Function to display usage information
usage() {
    echo "Usage: $0 <question_number>"
    exit 1
}

test() {
    number=$1
    # g++ $number.cpp --std=c++14 -o $number
    make QUESTION_ID=$number
    cat inputs/input_$number | ./build/$number
}

# Check the number of arguments
if [ "$#" -ne 1 ]; then
    echo "arguments number not correct: $# provided, 1 required."
    usage
else
    test $@
fi

