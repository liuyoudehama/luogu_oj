# Compiler
CXX = g++

# Default compiler flags
CXXFLAGS = -std=c++14 -Wall -g


TARGET = ./build/$(QUESTION_ID)
SRC = ./src/$(QUESTION_ID).cpp

# Default rule
all: $(TARGET)

# Link the target executable
$(TARGET): $(SRC)
	$(CXX) $(CXXFLAGS) -o $@ $^
	

# Clean up object files and the target executable
clean:
	rm -f $(OBJS) $(TARGET)

# Phony targets
.PHONY: all clean
